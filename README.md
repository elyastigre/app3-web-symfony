!! Initialisation !!

Lancer le projet nécessite l'installation de Symfony (https://symfony.com/download), et de composer (https://getcomposer.org)
(==> A verifier mais composer est peut être déjà disponible dans le CLI de Symfony)

Afin de télécharger les dépendances du projet, il faut lancer la commande `composer install` a la racine du projet.
(==> `symfony composer install` si composer est effectivement disponible dans la CLI de symfony)

Avant de lancer le serveur, il est nécessaire de créer un fichier .env.local (copie de .env) et de commenter/décommenter les bonnes lignes sous "doctrine/doctrine-bundle" afin de paramétrer la base de donnée.
(On peut aussi désactiver le mode DEBUG si il est activé : `APP_DEBUG=0`)
On crée ensuite la base de donnée avec `php bin/console doctrine:database:create`
Enfin, lancer `php bin/console doctrine:migrations:migrate` à la racine du projet permet de créer les tables nécessaires au fonctionnement du projet. 

Pour lancer le serveur, la commande `symfony server:start` doit être lancée sur la racine du projet (même dossier que ce README.me)
[Si le port 8000 est déjà occupé, il faut lancer `symfony server:start --port=4444`]