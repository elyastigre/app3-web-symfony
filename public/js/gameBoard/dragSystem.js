function Drag(pid) {
    var pions = document.getElementsByClassName("pion");

    var trouve = false;

    for (let pion of pions) {

        if (pion.id == "draggable" + pid) {
            var p = pion.id;

            $("#" + p).draggable({
                stop: function (event, ui) {
                    Bougepion(this.id, pion.style);
                }
            });

            $("#" + p).addClass("movable");
            trouve = true;
        }
    }

    // Reload la page lorsque le pion est créé mais pas montré.
    if (trouve == false) {
        location.reload();
    }
}

function DragAll() {
    var pions = document.getElementsByClassName("pion");

    for (let pion of pions) {
        p = pion.id;

        $("#" + p).draggable({
            stop: function (event, ui) {
                Bougepion(this.id, pion.style);
            }
        });

        $("#" + p).addClass("movable");
    }
}

function Bougepion(pid, style) {
    $.ajax({
        type: 'POST',
        url: 'bougePion',
        dataType: 'json',
        data: {
            'id': pid,
            'position': style.position,
            'positionx': style.left,
            'positiony': style.top
        },
        success: function () {
            console.log("Movement : Success");
        },

    }).done(function (response) {
        UpdatePions()
        console.log(response)
    });
}

function UpdatePions() {
    $.ajax({
        type: 'POST',
        url: 'updatePions',
        success: function () {
            console.log("Les pions ont tous été récupéré");
        }
    }).done(function (response) {
        console.log("UpdatePions : ");

        for (let numeropion in response['pions']) {
            var P = response['pions'][numeropion];
            console.log("idJoeuur : " + P['idJoueur']);
            console.log("x : " + P['x']);
            console.log("#draggable" + P['idJoueur']);

            // console.log($("#draggable" + P['idJoueur']).css('x'));
            // $("#draggable19").css('left', '247px');

            $("#draggable" + P['idJoueur']).css("left", P['x'] + "px").css("top", P['y'] + "px");
            // $("#draggable19").css('x', '247px');
        }
    })
}

function UpdateClock() {
    var intervalId = window.setInterval(function () {
        UpdatePions()
    }, 5000);
}