<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220311160351 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pion ALTER x TYPE INT');
        $this->addSql('ALTER TABLE pion ALTER x DROP DEFAULT');
        $this->addSql('ALTER TABLE pion ALTER y TYPE INT');
        $this->addSql('ALTER TABLE pion ALTER y DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE pion ALTER x TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE pion ALTER x DROP DEFAULT');
        $this->addSql('ALTER TABLE pion ALTER y TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE pion ALTER y DROP DEFAULT');
    }
}
