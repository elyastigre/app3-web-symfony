<?php

namespace App\Controller;

use App\Entity\Pion;
use App\Entity\Joueur;
use App\Repository\PionRepository;
use App\Repository\JoueurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\Serializer;

class GameController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security, ManagerRegistry $doctrine)
    {
        $this->security = $security;
        $this->doctrine = $doctrine;
    }

    #[Route('/game', name: 'game')]
    public function index(JoueurRepository $joueurRepository, PionRepository $pionRepository, EntityManagerInterface $manager): Response
    {
        // Vérifie si un utilisateur est connecté, renvoie à la page de login sinon
        $user = $this->security->getUser();
        if ($user == null) {
            return $this->redirectToRoute('security_login');
        }

        $joueur = $joueurRepository->findOneByUsername($user->getUserIdentifier());
        $id     = $joueur->getId();
        $pions  = $pionRepository->findAll();


        // On vérifie si le joueur a des pions. Si il n'en a pas et qu'il est gameMaster, en crée un en DB
        if (!$joueur->getIsGameMaster()) {
            $pionJoueur = $joueur->getPions();

            if (count($pionJoueur) == 0) {
                $pion = new Pion();
                $pion->setJoueur($joueur)
                    ->setX(0)
                    ->setY(0)
                    ->setIsNpc(false);

                $manager->persist($pion);
                $manager->flush();
            }
        }

        return $this->render('game/gameSession/gameBoard.html.twig', [
            'id'              => $id,
            'joueur'          => $joueur,
            'isGM'            => $joueur->getIsGameMaster(),
            'pions'           => $pions,
        ]);
    }

    #[Route('/bougePion', name: 'bouge_pion')]
    public function createPion(Request $request, PionRepository $pionRepository, EntityManagerInterface $manager, JoueurRepository $joueurRepository): Response
    {
        $joueur = $joueurRepository->find(intval(trim($request->request->get('id'), "draggable")));
        $pion = $pionRepository->findOneBy(['joueur' => $joueur]);

        $X = intval(trim($request->request->get('positionx'), "px"));
        $Y = intval(trim($request->request->get('positiony'), "px"));

        $pion->setXY($X, $Y);

        $manager->persist($pion);
        $manager->flush();


        return $this->json([
            'code' => 200,
            'message' => "Pion bougé !!",
            'test'  =>  $request->request->all(),
        ], 200);
    }

    #[Route('/updatePions', name: 'update_pions')]
    public function updatePions(Request $request, PionRepository $pionRepository)
    {
        $pions = $pionRepository->findAll();

        foreach ($pions as &$pion) {
            // Solution temporaire pour serializer les pions
            $pion->setNomJoueur($pion->getJoueur()->getUsername());
            $pion->setIdJoueur($pion->getJoueur()->getId());
            $pion->setJoueur(null);
        }

        return $this->json([
            'code'      => 200,
            'message'   => "La position des pions a été envoyée",
            'pions'     => $pions,
            'request'   => $request->request->all()
        ]);
    }
}
