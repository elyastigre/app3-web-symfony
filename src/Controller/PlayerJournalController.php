<?php

namespace App\Controller;

use App\Entity\FicheDePersonnage;
use App\Repository\JoueurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PlayerJournalController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security, ManagerRegistry $doctrine, EntityManagerInterface $manager)
    {
        $this->security = $security;
        $this->doctrine = $doctrine;
        $this->manager  = $manager;
    }

    #[Route('/PlayerJournal/{id}', name: 'player_journal')]
    public function PlayerJournal($id, JoueurRepository $repoJoueurs): Response
    {
        // Vérifie si un utilisateur est connecté, renvoie à la page de login sinon
        $user = $this->security->getUser();
        if ($user == null) {
            return $this->redirectToRoute('security_login');
        }

        // Trouve le joueur duquel on veut voir la page
        $joueur = $repoJoueurs->find($id);


        // Vérifie si l'utilisateur connecté (à ne pas confondre avec $joueur que l'on regarde) a le droit de regarder les notes de la page
        // afficheNote permet aussi d'afficher le bouton "Edit"
        $spectateur = $repoJoueurs->findOneByUsername($user->getUserIdentifier());
        $afficheNote = false;

        if ($spectateur->getIsGameMaster() || ($spectateur->getId() == $id)) {
            $afficheNote = true;
        }

        // Vérifie que le joueur a une fiche de personnage
        if ($joueur->getFicheDePersonnage() == null) {
            // S'il n'en a pas, la crée
            $playerSheet = new FicheDePersonnage();
            $playerSheet->setSTR(10)
                ->setDEX(10)
                ->setCON(10)
                ->setINT(10)
                ->setWIS(10)
                ->setCHA(10)
                ->setArgent(10)
                ->setPVMax(10)
                ->setPV(10)
                ->setBackground("Un aventurier, ou une aventurière peut être ? Qui aime se perdre et aider son prochain, ou qui va droit au but et n'aura aucune hésitation lorsqu'il faudra couper des têtes ? \nVotre aventure reste à écrire, mais ici résidra votre passé.")
                ->setInventory("Une bourse,\nUn sac à dos assez lourd rempli d'un sac de couchage et de vivres pour une semaine")
                ->setNotes("Ici, vos notes personnelles : seuls vous et les Maîtres de Jeu pourront les lire !")
                ->setJoueur($joueur);

            $this->manager->persist($playerSheet);
            $this->manager->flush();
        }
        else
        {
            $playerSheet = $joueur->getFicheDePersonnage();
        }

        return $this->render('game/playerJournal/journal.html.twig', [
            'id'              => $id,
            'username'        => $joueur->getUsername(),
            'FOR'             => $playerSheet->getSTR(),
            'DEX'             => $playerSheet->getDEX(),
            'CON'             => $playerSheet->getCON(),
            'INT'             => $playerSheet->getINT(),
            'SAG'             => $playerSheet->getWIS(),
            'CHA'             => $playerSheet->getCHA(),
            'Background'      => $playerSheet->getBackground(),
            'Inventory'       => $playerSheet->getInventory(),
            'afficheNote'     => $afficheNote,
            'Notes'           => $playerSheet->getNotes(),
            'Bourse'          => $playerSheet->getArgent(),
            'PV'              => $playerSheet->getPV(),
            'PVMax'           => $playerSheet->getPVMax(),
        ]);
    }

    #[Route('/PlayerJournal/edit/{id}', name: 'edit')]
    public function edit($id, JoueurRepository $repoJoueurs, Request $request): Response
    {
        // Vérifie si un utilisateur est connecté, renvoie à la page de login sinon
        $user = $this->security->getUser();
        if ($user == null) {
            return $this->redirectToRoute('security_login');
        }

        // Trouve le joueur duquel on veut voir la page
        $joueur = $repoJoueurs->find($id);

        // Vérifie si l'utilisateur connecté (à ne pas confondre avec $joueur que l'on regarde) a le droit de modifier de la page
        $spectateur = $repoJoueurs->findOneByUsername($user->getUserIdentifier());

        if (!($spectateur->getIsGameMaster()) && !($spectateur->getId() == $id)) {
            // Dans le cas où il n'a pas le droit, il est renvoyé vers le jeu
            return $this->redirectToRoute('game');
        }

        // Création du formulaire
        $playerSheet = $joueur->getFicheDePersonnage();
        $form = $this->createFormBuilder($playerSheet)->add('STR')
        ->add('DEX')
        ->add('CON')
        ->add('INT')
        ->add('WIS')
        ->add('CHA')
        ->add('PV')
        ->add('PVMax')
        ->add('Background')
        ->add('Inventory')
        ->add('Notes')
        ->add('Argent')
        ->getForm();

        // On vérifie si la requête contient un formulaire, et le traite si oui
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $playerSheet->setArgent($form->getData()->getArgent())
                        ->setSTR($form->getData()->getSTR())
                        ->setDEX($form->getData()->getDEX())
                        ->setCON($form->getData()->getCON())
                        ->setINT($form->getData()->getINT())
                        ->setWIS($form->getData()->getWIS())
                        ->setCHA($form->getData()->getCHA())
                        ->setBackground($form->getData()->getBackground())
                        ->setInventory($form->getData()->getInventory())
                        ->setNotes($form->getData()->getNotes())
                        ->setPV($form->getData()->getPV())
                        ->setPVMax($form->getData()->getPVMax());
                        
            $this->manager->persist($playerSheet);
            $this->manager->flush();

            return $this->redirectToRoute('player_journal', ['id' => $id]);
        }
        

        return $this->render('game/playerJournal/editPage.html.twig', [
            'id'              => $id,
            'username'        => $joueur->getUsername(),
            'FOR'             => $playerSheet->getSTR(),
            'DEX'             => $playerSheet->getDEX(),
            'CON'             => $playerSheet->getCON(),
            'INT'             => $playerSheet->getINT(),
            'SAG'             => $playerSheet->getWIS(),
            'CHA'             => $playerSheet->getCHA(),
            'Background'      => $playerSheet->getBackground(),
            'Inventory'       => $playerSheet->getInventory(),
            'Notes'           => $playerSheet->getNotes(),
            'Bourse'          => $playerSheet->getArgent(),
            'PV'              => $playerSheet->getPV(),
            'PVMax'           => $playerSheet->getPVMax(),
            'formEdit'        => $form->createView(),
        ]);
    }

    /**
     * Permet de trouver un joueur via son nom sur la barre de recherche
     */
    #[Route('/search', name: 'SearchBar')]
    public function search(JoueurRepository $repoJoueurs, Request $request): Response
    {
        if ($repoJoueurs->findOneByUsername($request->query->get('username')) != null)
        {
            $id = $repoJoueurs->findOneByUsername($request->query->get('username'))->getId();
        }
        else $id = null;
        
        if ($id != null)
        {
            return $this->redirectToRoute('player_journal', ['id' => $id]);
        }
        else
        {
            return $this->redirectToRoute('game');
        }
    }
}
