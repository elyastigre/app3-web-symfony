<?php

namespace App\Controller;

use App\Entity\Joueur;
use App\Form\ResgistrationType;
// use Doctrine\Persistence\ObjectManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class SecurityController extends AbstractController
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    #[Route('/signup', name: 'security_signup')]
    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordHasherInterface $passwordHasher): Response
    {
        // Vérifie si un utilisateur est connecté, renvoie à la page de jeu si oui
        $user = $this->security->getUser();
        if ($user != null) {
            return $this->redirectToRoute('game');
        }


        $joueur = new Joueur();

        $form = $this->createForm(ResgistrationType::class, $joueur);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $hashedPassword = $passwordHasher->hashPassword($joueur, $joueur->getPassword());

            $joueur->setPassword($hashedPassword);

            $manager->persist($joueur);
            $manager->flush();

            return $this->redirectToRoute('security_login');
        }

        return $this->render('security/signup.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/login', name: 'security_login')]
    public function login(): Response
    {
        // Vérifie si un utilisateur est connecté, renvoie à la page de jeu si oui
        $user = $this->security->getUser();
        if ($user != null) {
            return $this->redirectToRoute('game');
        }

        $joueur = new Joueur();

        $form = $this->createFormBuilder($joueur)
            ->add('username', $type = null, [
                'label'     => false,
                'required'  => true,
                'attr'      => [
                    'placeholder' => "Nom d'utilisateur ",
                ]
            ])
            ->add('password', $type = PasswordType::class, [
                'label'     => false,
                'required'  => true,
                'attr'      => [
                    'placeholder'   => "Mot de passe",
                ],
            ])
            ->getForm();

        return $this->render('security/login.html.twig', [
            'formJoueur' => $form->createView()
        ]);
    }

    #[Route('/logout', name: 'security_logout')]
    public function logout()
    {
    }

    #[Route('/', name: 'home')]
    public function home(): Response
    {
        return $this->redirectToRoute("security_login");
    }
}
