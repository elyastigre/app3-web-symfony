<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\FicheDePersonnageRepository;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: FicheDePersonnageRepository::class)]
class FicheDePersonnage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\OneToOne(inversedBy: 'ficheDePersonnage', targetEntity: Joueur::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    private $Joueur;

    #[ORM\Column(type: 'integer')]
    #[Assert\LessThanOrEqual(value: 20)]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private $STR;

    #[ORM\Column(type: 'integer')]
    #[Assert\LessThanOrEqual(value: 20)]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private $DEX;

    #[ORM\Column(type: 'integer')]
    #[Assert\LessThanOrEqual(value: 20)]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private $CON;

    #[ORM\Column(type: 'integer')]
    #[Assert\GreaterThanOrEqual(value: 0)]
    #[Assert\LessThanOrEqual(value: 20)]
    private $INT;

    #[ORM\Column(type: 'integer')]
    #[Assert\LessThanOrEqual(value: 20)]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private $WIS;

    #[ORM\Column(type: 'integer')]
    #[Assert\LessThanOrEqual(value: 20)]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private $CHA;

    #[ORM\Column(type: 'integer')]
    #[Assert\Positive]
    #[Assert\LessThanOrEqual(propertyPath: "PVMax")]
    private $PV;

    #[ORM\Column(type: 'integer')]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private $PVMax;

    #[ORM\Column(type: 'text', nullable: true)]
    private $Background;

    #[ORM\Column(type: 'text', nullable: true)]
    private $Inventory;

    #[ORM\Column(type: 'text', nullable: true)]
    private $Notes;

    #[ORM\Column(type: 'integer')]
    #[Assert\GreaterThanOrEqual(value: 0)]
    private $Argent;

    public function getArgent(): ?int
    {
        return $this->Argent;
    }

    public function setArgent(int $Argent): self
    {
        $this->Argent = $Argent;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJoueur(): ?Joueur
    {
        return $this->Joueur;
    }

    public function setJoueur(Joueur $Joueur): self
    {
        $this->Joueur = $Joueur;

        return $this;
    }

    public function getSTR(): ?int
    {
        return $this->STR;
    }

    public function setSTR(int $STR): self
    {
        $this->STR = $STR;

        return $this;
    }

    public function getDEX(): ?int
    {
        return $this->DEX;
    }

    public function setDEX(int $DEX): self
    {
        $this->DEX = $DEX;

        return $this;
    }

    public function getCON(): ?int
    {
        return $this->CON;
    }

    public function setCON(int $CON): self
    {
        $this->CON = $CON;

        return $this;
    }

    public function getINT(): ?int
    {
        return $this->INT;
    }

    public function setINT(int $INT): self
    {
        $this->INT = $INT;

        return $this;
    }

    public function getWIS(): ?int
    {
        return $this->WIS;
    }

    public function setWIS(int $WIS): self
    {
        $this->WIS = $WIS;

        return $this;
    }

    public function getCHA(): ?int
    {
        return $this->CHA;
    }

    public function setCHA(int $CHA): self
    {
        $this->CHA = $CHA;

        return $this;
    }

    public function getPV(): ?int
    {
        return $this->PV;
    }

    public function setPV(int $PV): self
    {
        $this->PV = $PV;

        return $this;
    }

    public function getPVMax(): ?int
    {
        return $this->PVMax;
    }

    public function setPVMax(int $PVMax): self
    {
        $this->PVMax = $PVMax;

        return $this;
    }

    public function getBackground(): ?string
    {
        return $this->Background;
    }

    public function setBackground(?string $Background): self
    {
        $this->Background = $Background;

        return $this;
    }

    public function getInventory(): ?string
    {
        return $this->Inventory;
    }

    public function setInventory(?string $Inventory): self
    {
        $this->Inventory = $Inventory;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->Notes;
    }

    public function setNotes(?string $Notes): self
    {
        $this->Notes = $Notes;

        return $this;
    }
}
