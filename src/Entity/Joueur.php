<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\JoueurRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: JoueurRepository::class)]
#[UniqueEntity(
    fields: ["username"],
    message: 'Le Nom indiqué est déjà utilisé ! Un peu d\'originalité Bon Sang !!!'
)]
class Joueur implements PasswordAuthenticatedUserInterface, UserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'boolean')]
    private $isGameMaster;

    #[ORM\Column(type: 'string', length: 255)]
    private $username;

    #[ORM\Column(type: 'string', length: 255)]
    #[Assert\Length(min: 8, minMessage: 'Votre mot de passe doit faire minimum 8 caractères')]
    private $password;

    #[Assert\EqualTo(propertyPath: "password", message: "Mot de passe mal confirmé")]
    private $confirm_password;

    #[ORM\OneToOne(mappedBy: 'Joueur', targetEntity: FicheDePersonnage::class, cascade: ['persist', 'remove'])]
    private $ficheDePersonnage;

    #[ORM\OneToMany(mappedBy: 'joueur', targetEntity: Pion::class)]
    private $pions;

    public function __construct()
    {
        $this->pions = new ArrayCollection();
    }

    public function getConfirmPassword(): ?string
    {
        return $this->confirm_password;
    }

    public function setConfirmPassword(string $confirm_password)
    {
        $this->confirm_password = $confirm_password;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsGameMaster(): ?bool
    {
        return $this->isGameMaster;
    }

    public function setIsGameMaster(bool $isGameMaster): self
    {
        $this->isGameMaster = $isGameMaster;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    public function getIdentifier(): string
    {
        return $this->getUsername();
    }

    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    public function eraseCredentials()
    {
    }

    public function getSalt()
    {
    }

    public function getFicheDePersonnage(): ?FicheDePersonnage
    {
        return $this->ficheDePersonnage;
    }

    public function setFicheDePersonnage(FicheDePersonnage $ficheDePersonnage): self
    {
        // set the owning side of the relation if necessary
        if ($ficheDePersonnage->getJoueur() !== $this) {
            $ficheDePersonnage->setJoueur($this);
        }

        $this->ficheDePersonnage = $ficheDePersonnage;

        return $this;
    }

    /**
     * @return Collection|Pion[]
     */
    public function getPions(): Collection
    {
        return $this->pions;
    }

    public function addPion(Pion $pion): self
    {
        if (!$this->pions->contains($pion)) {
            $this->pions[] = $pion;
            $pion->setJoueur($this);
        }

        return $this;
    }

    public function removePion(Pion $pion): self
    {
        if ($this->pions->removeElement($pion)) {
            // set the owning side to null (unless already changed)
            if ($pion->getJoueur() === $this) {
                $pion->setJoueur(null);
            }
        }

        return $this;
    }
}
