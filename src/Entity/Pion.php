<?php

namespace App\Entity;

use App\Repository\PionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PionRepository::class)]
class Pion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'integer')]
    private $x;

    #[ORM\Column(type: 'integer')]
    private $y;

    #[ORM\ManyToOne(targetEntity: Joueur::class, inversedBy: 'pions')]
    #[ORM\JoinColumn(nullable: false)]
    private $joueur;

    // Pas en BD, sert au niveau de GameController/updatePions
    private $nom_joueur;
    private $id_joueur;

    #[ORM\Column(type: 'boolean')]
    private $isNpc;

    public function getNomJoueur(): ?string
    {
        return $this->nom_joueur;
    }

    public function setNomJoueur($nom_joueur): self
    {
        $this->nom_joueur = $nom_joueur;

        return $this;
    }

    public function getIdJoueur(): ?int
    {
        return $this->id_joueur;
    }

    public function setIdJoueur($id_joueur): ?self
    {
        $this->id_joueur = $id_joueur;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getX(): ?float
    {
        return $this->x;
    }

    public function setX(float $x): self
    {
        $this->x = $x;

        return $this;
    }

    public function setXY(int $x, int $y): self
    {
        $this->x = $x;
        $this->y = $y;

        return $this;
    }

    public function getY(): ?float
    {
        return $this->y;
    }

    public function setY(float $y): self
    {
        $this->y = $y;

        return $this;
    }

    public function getJoueur(): ?Joueur
    {
        return $this->joueur;
    }

    public function setJoueur(?Joueur $joueur): self
    {
        $this->joueur = $joueur;

        return $this;
    }

    public function getIsNpc(): ?bool
    {
        return $this->isNpc;
    }

    public function setIsNpc(bool $isNpc): self
    {
        $this->isNpc = $isNpc;

        return $this;
    }
}
