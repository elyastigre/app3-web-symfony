<?php

namespace App\Repository;

use App\Entity\FicheDePersonnage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FicheDePersonnage|null find($id, $lockMode = null, $lockVersion = null)
 * @method FicheDePersonnage|null findOneBy(array $criteria, array $orderBy = null)
 * @method FicheDePersonnage[]    findAll()
 * @method FicheDePersonnage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FicheDePersonnageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FicheDePersonnage::class);
    }

    // /**
    //  * @return FicheDePersonnage[] Returns an array of FicheDePersonnage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FicheDePersonnage
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
